package com.example.clicklabs124.studentmanagementsystem;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.database.StudentDatabase;
import com.example.util.*;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;


public class addStudent extends Activity {

    Button button;
    EditText name;
    EditText rollNo;
    private int LOAD_IMAGE_RESULTS = 1;
    private int REQUEST_TAKE_PHOTO = 3;
    StudentDatabase database;
    private Button imageButton;
    private ImageView image;
    String imagePath = null;
    boolean asyncFlag, serviceFlag, intentServiceFlag, dbFlag = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_student);
        image = (ImageView) findViewById(R.id.image);
        image.setImageResource(R.mipmap.profile);
        Intent intent = getIntent();
        asyncFlag = intent.getExtras().getBoolean("asyncFlag");
        serviceFlag = intent.getExtras().getBoolean("serviceFlag");
        intentServiceFlag = intent.getExtras().getBoolean("intentServiceFlag");
        database = new StudentDatabase(this);
        addListenerOnButton();
    }


    private void addListenerOnButton() {

        button = (Button) findViewById(R.id.addStudent);

        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {


                name = (EditText) findViewById(R.id.studentName);
                rollNo = (EditText) findViewById(R.id.roll);
                if (asyncFlag) {
                    InsertDataTask obj = new InsertDataTask();
                    obj.execute();

                } else if (serviceFlag) {
                    Intent intent;
                    intent = new Intent(addStudent.this, DeleteDataService.class);
                    intent.putExtra("studentName", name.getText().toString());
                    intent.putExtra("roll", rollNo.getText().toString());
                    intent.putExtra("imagePath", imagePath);
                    intent.putExtra("action", "add");
                    startService(intent);
                    // setResult(AppConstants.RESULT_REQUEST_FOR_SAVE, intent);
                    finish();

                } else if (intentServiceFlag) {
                    Intent intent;
                    intent = new Intent(addStudent.this, MyIntentService.class);
                    intent.putExtra("studentName", name.getText().toString());
                    intent.putExtra("roll", rollNo.getText().toString());
                    intent.putExtra("imagePath", imagePath);
                    intent.putExtra("action", "add");
                    startService(intent);
                    // setResult(AppConstants.RESULT_REQUEST_FOR_SAVE, intent);
                    finish();
                }
            }

        });

        imageButton = (Button) findViewById(R.id.addImageButton);
        imageButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                selectImage();

            }
        });
    }

    void selectImage() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Add Profile Image");
        dialog.setItems(R.array.image_menu, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, REQUEST_TAKE_PHOTO);

                } else if (item == 1) {
                    Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(i, LOAD_IMAGE_RESULTS);
                } else {
                    dialog.dismiss();
                }
            }
        });
        dialog.show();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LOAD_IMAGE_RESULTS && resultCode == RESULT_OK && data != null) {
            Uri pickedImage = data.getData();
            String[] filePath = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(pickedImage, filePath, null, null, null);
            cursor.moveToFirst();
            imagePath = cursor.getString(cursor.getColumnIndex(filePath[0]));
            image.setImageBitmap(BitmapFactory.decodeFile(imagePath));
            cursor.close();
        }
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

            File destination = new File(Environment.getExternalStorageDirectory(),
                    System.currentTimeMillis() + ".jpg");

            FileOutputStream fo;
            try {
                destination.createNewFile();
                fo = new FileOutputStream(destination);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            imagePath = destination.getAbsolutePath();
            image.setImageBitmap(thumbnail);
        }
    }

    public class InsertDataTask extends AsyncTask<Void, Void, Void> {

        private final ProgressDialog dialog = new ProgressDialog(
                addStudent.this);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.dialog.setMessage("Inserting data...");
            this.dialog.show();

        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                Thread.sleep(500);
            } catch (Exception ae) {
            }

            String studentName = name.getText().toString();
            String studentImagePath = imagePath;
            Integer studentRollNo = Integer.parseInt(rollNo.getText().toString());
            dbFlag = database.insertContact(studentRollNo, studentName, studentImagePath);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Intent intent = new Intent(addStudent.this, MainActivity.class);
            this.dialog.dismiss();
            intent.putExtra("studentName", name.getText().toString());
            intent.putExtra("roll", rollNo.getText().toString());
            intent.putExtra("imagePath", imagePath);
            intent.putExtra("flag", dbFlag);
            setResult(AppConstants.RESULT_REQUEST_FOR_SAVE, intent);
            finish();
        }
    }

}
