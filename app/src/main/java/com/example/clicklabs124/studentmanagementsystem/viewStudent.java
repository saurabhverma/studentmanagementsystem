package com.example.clicklabs124.studentmanagementsystem;

import android.app.Activity;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;


public class viewStudent extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_student);
        Intent intent = getIntent();
        String studentName = intent.getExtras().getString("studentName");
        String rollNo = intent.getExtras().getString("roll");
        String imagePath = intent.getExtras().getString("imagePath");
        TextView roll = (TextView) findViewById(R.id.roll);
        TextView name = (TextView) findViewById(R.id.name);
        ImageView image = (ImageView) findViewById(R.id.image);
        roll.setText(rollNo);
        name.setText(studentName);
        if (imagePath == null) {
            image.setImageResource(R.mipmap.profile);
        } else {
            image.setImageBitmap(BitmapFactory.decodeFile(imagePath));
        }

    }

}
