package com.example.clicklabs124.studentmanagementsystem;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.widget.Toast;

import com.example.database.StudentDatabase;
import com.example.util.AppConstants;


public class MyIntentService extends IntentService {

    StudentDatabase database;
    int roll;
    boolean flag;

    public MyIntentService() {
        super("MyIntentService");
        database = new StudentDatabase(this);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String action = intent.getStringExtra("action");

        if (action.equals("delete")) {
            roll = intent.getExtras().getInt("roll");
            database.deleteContact(roll);
            broadcastIntent(intent, action);
        } else if (action.equals("add")) {
            String studentName = intent.getExtras().getString("studentName");
            String studentImagePath = intent.getExtras().getString("imagePath");
            Integer studentRollNo = Integer.parseInt(intent.getExtras().getString("roll"));
            if (database.insertContact(studentRollNo, studentName, studentImagePath)) {
                Intent intent1 = new Intent(this, MainActivity.class);
                intent1.putExtras(intent);
                broadcastIntent(intent1, action);
            }

        } else if (action.equals("edit")) {
            String studentName = intent.getExtras().getString("studentName");
            String studentImagePath = intent.getExtras().getString("imagePath");
            Integer studentRollNo = Integer.parseInt(intent.getExtras().getString("roll"));
            Integer roll_To_Update = intent.getExtras().getInt("rollToUpdate");
            if (database.updateContact(roll_To_Update, studentRollNo, studentName, studentImagePath)) {
                Intent intent1 = new Intent(this, MainActivity.class);
                intent1.putExtras(intent);
                broadcastIntent(intent1, action);
            }
        }
    }

    void broadcastIntent(Intent intent1, String action) {
        Intent broadcastIntent = new Intent();
        broadcastIntent.putExtras(intent1);
        broadcastIntent.putExtra("action", action);
        broadcastIntent.setAction("UPDATE DATABASE");
        sendBroadcast(broadcastIntent);
    }

}
