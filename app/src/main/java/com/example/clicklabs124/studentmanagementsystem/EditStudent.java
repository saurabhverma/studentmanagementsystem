package com.example.clicklabs124.studentmanagementsystem;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.database.StudentDatabase;
import com.example.util.*;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;


public class EditStudent extends Activity {

    Button button;
    EditText name;
    EditText rollNo;
    private int LOAD_IMAGE_RESULTS = 1;
    private int REQUEST_TAKE_PHOTO = 3;
    StudentDatabase database;
    private Button imageButton;
    private ImageView image;
    String imagePath;
    boolean asyncFlag, serviceFlag, intentServiceFlag;
    Integer roll_To_Update;
    boolean dbFlag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_student);
        Intent intent = getIntent();
        asyncFlag = intent.getExtras().getBoolean("asyncFlag");
        serviceFlag = intent.getExtras().getBoolean("serviceFlag");
        intentServiceFlag = intent.getExtras().getBoolean("intentServiceFlag");
        roll_To_Update = intent.getExtras().getInt("rollToUpdate");
        database = new StudentDatabase(this);
        String studentName = intent.getExtras().getString("studentName");
        String rollNo = intent.getExtras().getString("roll");
        imagePath = intent.getExtras().getString("imagePath");
        EditText roll = (EditText) findViewById(R.id.roll);
        EditText name = (EditText) findViewById(R.id.studentName);
        ImageView image = (ImageView) findViewById(R.id.image);
        roll.setText(rollNo);
        name.setText(studentName);
        if (imagePath == null) {
            image.setImageResource(R.mipmap.profile);
        } else {
            image.setImageBitmap(BitmapFactory.decodeFile(imagePath));
        }
        addListenerOnButton();
    }

    private void addListenerOnButton() {

        button = (Button) findViewById(R.id.addStudent);
        name = (EditText) findViewById(R.id.studentName);
        rollNo = (EditText) findViewById(R.id.roll);

        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (asyncFlag) {
                    EditDataTask obj = new EditDataTask();
                    obj.execute();

                } else if (serviceFlag) {
                    Intent intent;
                    intent = new Intent(EditStudent.this, DeleteDataService.class);
                    intent.putExtra("rollToUpdate", roll_To_Update);
                    intent.putExtra("studentName", name.getText().toString());
                    intent.putExtra("roll", rollNo.getText().toString());
                    intent.putExtra("imagePath", imagePath);
                    intent.putExtra("action", "edit");
                    startService(intent);
                    //setResult(AppConstants.RESULT_REQUEST_FOR_UPDATE, intent);
                    finish();

                } else if (intentServiceFlag) {
                    Intent intent;
                    intent = new Intent(EditStudent.this, MyIntentService.class);
                    intent.putExtra("rollToUpdate", roll_To_Update);
                    intent.putExtra("studentName", name.getText().toString());
                    intent.putExtra("roll", rollNo.getText().toString());
                    intent.putExtra("imagePath", imagePath);
                    intent.putExtra("action", "edit");
                    startService(intent);
                    finish();

                }/*else {

                    Intent intent = new Intent(EditStudent.this, MainActivity.class);
                    intent.putExtra("studentName", name.getText().toString());
                    intent.putExtra("roll", rollNo.getText().toString());
                    intent.putExtra("imagePath", imagePath);
                    setResult(AppConstants.RESULT_REQUEST_FOR_UPDATE, intent);
                    finish();
                }*/
            }
        });

        imageButton = (Button) findViewById(R.id.addImageButton);
        imageButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                selectImage();
            }
        });

    }

    void selectImage() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Add Profile Image");
        dialog.setItems(R.array.image_menu, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, REQUEST_TAKE_PHOTO);
                } else if (item == 1) {
                    Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(i, LOAD_IMAGE_RESULTS);
                } else {
                    dialog.dismiss();
                }
            }
        });
        dialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LOAD_IMAGE_RESULTS && resultCode == RESULT_OK && data != null) {
            Uri pickedImage = data.getData();
            String[] filePath = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(pickedImage, filePath, null, null, null);
            cursor.moveToFirst();
            imagePath = cursor.getString(cursor.getColumnIndex(filePath[0]));
            image = (ImageView) findViewById(R.id.image);
            image.setImageBitmap(BitmapFactory.decodeFile(imagePath));
            cursor.close();
        }
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            thumbnail.compress(Bitmap.CompressFormat.JPEG, 60, bytes);

            File destination = new File(Environment.getExternalStorageDirectory(),
                    System.currentTimeMillis() + ".jpg");

            FileOutputStream fo;
            try {
                destination.createNewFile();
                fo = new FileOutputStream(destination);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (FileNotFoundException exception) {
                exception.printStackTrace();
            } catch (IOException exception) {
                exception.printStackTrace();
            }
            imagePath = destination.getAbsolutePath();
            image = (ImageView) findViewById(R.id.image);
            image.setImageBitmap(thumbnail);
        }
    }

    public class EditDataTask extends AsyncTask<Void, Void, Void> {

        private final ProgressDialog dialog = new ProgressDialog(
                EditStudent.this);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.dialog.setMessage("Updating data...");
            this.dialog.show();

        }

        @Override
        protected Void doInBackground(Void... params) {

            String studentName = name.getText().toString();
            String studentImagePath = imagePath;
            Integer studentRollNo = Integer.parseInt(rollNo.getText().toString());
            dbFlag = database.updateContact(roll_To_Update, studentRollNo, studentName, studentImagePath);
            try {
                Thread.sleep(500);
            } catch (Exception ae) {
            }
            //Toast.makeText(getApplicationContext(), "Updated", Toast.LENGTH_LONG).show();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Intent intent = new Intent(EditStudent.this, MainActivity.class);
            this.dialog.dismiss();
            intent.putExtra("studentName", name.getText().toString());
            intent.putExtra("roll", rollNo.getText().toString());
            intent.putExtra("imagePath", imagePath);
            intent.putExtra("flag", dbFlag);
            setResult(AppConstants.RESULT_REQUEST_FOR_UPDATE, intent);
            finish();
        }
    }

}
