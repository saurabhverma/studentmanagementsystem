package com.example.clicklabs124.studentmanagementsystem;

import android.app.ProgressDialog;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.widget.Toast;

import com.example.database.StudentDatabase;


public class DeleteDataService extends Service {
    StudentDatabase database;
    int roll;
    boolean flag;

    public DeleteDataService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        database = new StudentDatabase(this);


    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            String action = intent.getStringExtra("action");


            if (action.equals("delete")) {
                roll = intent.getExtras().getInt("roll");
                database.deleteContact(roll);
                broadcastIntent(intent, action);
            } else if (action.equals("add")) {
                String studentName = intent.getExtras().getString("studentName");
                String studentImagePath = intent.getExtras().getString("imagePath");
                Integer studentRollNo = Integer.parseInt(intent.getExtras().getString("roll"));
                if(flag=database.insertContact(studentRollNo, studentName, studentImagePath))
                {
                    Intent intent1=new Intent(this,MainActivity.class);
                    intent1.putExtras(intent);
                    intent1.putExtra("flag",flag);
                    broadcastIntent(intent1, action);
                }

            } else if (action.equals("edit")) {
                String studentName = intent.getExtras().getString("studentName");
                String studentImagePath = intent.getExtras().getString("imagePath");
                Integer studentRollNo = Integer.parseInt(intent.getExtras().getString("roll"));
                Integer roll_To_Update = intent.getExtras().getInt("rollToUpdate");
                if(database.updateContact(roll_To_Update, studentRollNo, studentName, studentImagePath)){
                    Intent intent1=new Intent(this,MainActivity.class);
                    intent1.putExtras(intent);
                    broadcastIntent(intent1,action);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return START_STICKY;

    }

    /*@Override
    public void onDestroy() {
        super.onDestroy();
        Intent broadcastIntent = new Intent();
        broadcastIntent.putExtra("flag", false);
        broadcastIntent.setAction("UPDATE DATABASE");
        sendBroadcast(broadcastIntent);
        //Toast.makeText(this, "Service Destroyed", Toast.LENGTH_LONG).show();
    }*/
    void broadcastIntent(Intent intent1,String action) {
        Intent broadcastIntent = new Intent();
        broadcastIntent.putExtras(intent1);
        broadcastIntent.putExtra("action",action);
        broadcastIntent.setAction("UPDATE DATABASE");
        sendBroadcast(broadcastIntent);
    }
}
