package com.example.clicklabs124.studentmanagementsystem;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Button;
import android.view.View.OnClickListener;
import android.content.Intent;
import android.view.View;
import android.widget.Spinner;
import android.widget.Toast;
import com.example.database.StudentDatabase;
import com.example.util.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MainActivity extends Activity implements AdapterView.OnItemSelectedListener {

    Button createButton, viewButton;
    public ArrayList<Student> studentData = new ArrayList<Student>();
    String name, imagePath;
    int rollNo;
    MyBaseAdapter adapter;
    ListView listView;
    GridView gridView;
    int menuSelectedItemIndex;
    Spinner spinner;
    StudentDatabase database;
    Integer roll_To_Update;
    boolean asyncFlag, serviceFlag, intentServiceFlag;
    MyReceiver receiver;
    IntentFilter filter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        database = new StudentDatabase(this);
        Cursor cursor = database.getAllCotacts();
        cursor.moveToFirst();
        try {
            do {
                Student studentObject = new Student(Integer.parseInt(cursor.getString(0)), cursor.getString(1), cursor.getString(2));
                studentData.add(studentObject);
            } while (cursor.moveToNext());
        } catch (CursorIndexOutOfBoundsException exception) {
            exception.fillInStackTrace();
        }
        spinner = (Spinner) findViewById(R.id.spinner);
        spinner.setOnItemSelectedListener(this);
        List<String> categories = new ArrayList<String>();
        categories.add("Sort");
        categories.add("By Name");
        categories.add("By Roll No.");
        ArrayAdapter<String> dropMenuAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);
        dropMenuAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dropMenuAdapter);
        addListenerOnButton();
        listView = (ListView) findViewById(R.id.studentList);
        adapter = new MyBaseAdapter(this, studentData);
        registerForContextMenu(listView);
        listView.setAdapter(adapter);
        gridView = (GridView) findViewById(R.id.gridView);
        gridView.setVisibility(View.INVISIBLE);
        registerForContextMenu(gridView);
        selectTask();
        filter = new IntentFilter("UPDATE DATABASE");
        receiver = new MyReceiver();
        registerReceiver(receiver, filter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String name = adapter.getItem(position).getName();
                roll_To_Update = adapter.getItem(position).getRoll();
                String roll = String.valueOf(adapter.getItem(position).getRoll());
                String imagePath = adapter.getItem(position).getImagePath();
                Intent intent;
                intent = new Intent(getApplicationContext(), viewStudent.class);
                intent.putExtra("studentName", name);
                intent.putExtra("roll", roll);
                intent.putExtra("imagePath", imagePath);
                startActivity(intent);
            }
        });
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String name = adapter.getItem(position).getName();
                roll_To_Update = adapter.getItem(position).getRoll();
                String roll = String.valueOf(adapter.getItem(position).getRoll());
                String imagePath = adapter.getItem(position).getImagePath();
                Intent intent;
                intent = new Intent(getApplicationContext(), viewStudent.class);
                intent.putExtra("studentName", name);
                intent.putExtra("roll", roll);
                intent.putExtra("imagePath", imagePath);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, filter);
    }

    void selectTask() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Select Background Task");
        dialog.setItems(R.array.task_selection_menu, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case AppConstants.ASYNCTASK:
                        asyncFlag = true;
                        break;
                    case AppConstants.SERVICE:
                        serviceFlag = true;
                        break;
                    case AppConstants.INTENTSERVICE:
                        intentServiceFlag = true;
                        break;
                    default:
                        break;
                }
            }
        });
        dialog.show();
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String item = parent.getItemAtPosition(position).toString();
        if (item.equals("By Name")) {
            sortName();
            Toast.makeText(parent.getContext(), "Sorted: " + item, Toast.LENGTH_SHORT).show();
        } else if (item.equals("By Roll No.")) {
            sortRoll();
            Toast.makeText(parent.getContext(), "Sorted: " + item, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Options");
        menu.add(0, AppConstants.VIEW, 1, "View");//groupId, itemId, order, title
        menu.add(0, AppConstants.EDIT, 2, "Edit");
        menu.add(0, AppConstants.DELETE, 3, "Delete");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        menuSelectedItemIndex = info.position;
        String name = adapter.getItem(info.position).getName();
        roll_To_Update = adapter.getItem(info.position).getRoll();
        String roll = String.valueOf(adapter.getItem(info.position).getRoll());
        String imagePath = adapter.getItem(info.position).getImagePath();
        Intent intent;
        switch (item.getItemId()) {
            case AppConstants.VIEW:
                intent = new Intent(this, viewStudent.class);
                intent.putExtra("studentName", name);
                intent.putExtra("roll", roll);
                intent.putExtra("imagePath", imagePath);
                startActivity(intent);
                return true;
            case AppConstants.EDIT:
                intent = new Intent(this, EditStudent.class);
                intent.putExtra("rollToUpdate", roll_To_Update);
                intent.putExtra("asyncFlag", asyncFlag);
                intent.putExtra("serviceFlag", serviceFlag);
                intent.putExtra("intentServiceFlag", intentServiceFlag);
                intent.putExtra("studentName", name);
                intent.putExtra("roll", roll);
                intent.putExtra("imagePath", imagePath);
                if(asyncFlag){
                    startActivityForResult(intent,AppConstants.RESULT_REQUEST_FOR_UPDATE);
                }
                else{
                startActivity(intent);
                }
                return true;
            case AppConstants.DELETE:
                if (asyncFlag) {
                    DeleteDataTask obj = new DeleteDataTask();
                    obj.execute();
                } else if (serviceFlag) {
                    intent = new Intent(this, DeleteDataService.class);
                    intent.putExtra("roll", roll_To_Update);
                    intent.putExtra("action", "delete");
                    startService(intent);
                    registerReceiver(receiver, filter);
                } else if (intentServiceFlag) {
                    intent = new Intent(this, MyIntentService.class);
                    intent.putExtra("roll", roll_To_Update);
                    intent.putExtra("action", "delete");
                    startService(intent);
                }
                return true;
            default:
                return true;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        name = intent.getExtras().getString("studentName");
        imagePath = intent.getExtras().getString("imagePath");
        rollNo = Integer.parseInt(intent.getExtras().getString("roll"));
        Student student = new Student(rollNo, name, imagePath);
        if (requestCode == AppConstants.RESULT_REQUEST_FOR_SAVE) {

            try {
                if (asyncFlag) {
                    if (intent.getExtras().getBoolean("flag")) {
                        studentData.add(student);
                        Toast.makeText(getApplicationContext(), "Saved Successfully", Toast.LENGTH_LONG).show();
                        adapter.notifyDataSetChanged();
                    } else {
                        Toast.makeText(getApplicationContext(), "Roll already exits!!!", Toast.LENGTH_LONG).show();
                    }

                }/* else if (serviceFlag) {
                    studentData.add(student);
                    adapter.notifyDataSetChanged();
                } /*else if (intentServiceFlag) {
                    studentData.add(student);
                    adapter.notifyDataSetChanged();
                }*/
            } catch (Exception ne) {
            }
        } else if (requestCode == AppConstants.RESULT_REQUEST_FOR_UPDATE) {
            try {
                if (asyncFlag) {
                    if (intent.getExtras().getBoolean("flag")) {
                        adapter.getItem(menuSelectedItemIndex).setName(name);
                        adapter.getItem(menuSelectedItemIndex).setImagePath(imagePath);
                        adapter.getItem(menuSelectedItemIndex).setRoll(rollNo);
                        Toast.makeText(getApplicationContext(), "Update Successfully", Toast.LENGTH_LONG).show();
                        adapter.notifyDataSetChanged();
                    } else {
                        Toast.makeText(getApplicationContext(), "Roll already exits!!!", Toast.LENGTH_LONG).show();
                    }
                }/* else if (serviceFlag) {
                    studentData = database.getUpdateList(studentData);
                    adapter.notifyDataSetChanged();
                } /*else if (intentServiceFlag) {
                    studentData = database.getUpdateList(studentData);
                    adapter.notifyDataSetChanged();

                    //studentData = database.getUpdateList(studentData);
                    // adapter.notifyDataSetChanged();
                }/*else {
                    name = intent.getExtras().getString("studentName");
                    rollNo = Integer.parseInt(intent.getExtras().getString("roll"));
                    imagePath = intent.getExtras().getString("imagePath");
                    adapter.getItem(menuSelectedItemIndex).name = name;
                    adapter.getItem(menuSelectedItemIndex).rollNo = rollNo;
                    adapter.getItem(menuSelectedItemIndex).imagePath = imagePath;
                    database.updateContact(roll_To_Update, rollNo, name, imagePath);
                    Toast.makeText(getApplicationContext(), "Updated", Toast.LENGTH_LONG).show();
                    adapter.notifyDataSetChanged();
                }*/
            } catch (NullPointerException ne) {
            }
        }
    }


    public void addListenerOnButton() {
        createButton = (Button) findViewById(R.id.createStudent);
        createButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(MainActivity.this, addStudent.class);
                intent.putExtra("asyncFlag", asyncFlag);
                intent.putExtra("serviceFlag", serviceFlag);
                intent.putExtra("intentServiceFlag", intentServiceFlag);
                if(asyncFlag){
                    startActivityForResult(intent, AppConstants.RESULT_REQUEST_FOR_SAVE);
                }else{startActivity(intent);}
            }
        });

        viewButton = (Button) findViewById(R.id.viewButton);
        viewButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (viewButton.getText().equals("Grid")) {
                    gridView.setAdapter(adapter);
                    gridView.setVisibility(View.VISIBLE);
                    listView.setVisibility(View.INVISIBLE);
                    viewButton.setText("List");

                } else if (viewButton.getText().equals("List")) {
                    listView.setAdapter(adapter);
                    listView.setVisibility(View.VISIBLE);
                    gridView.setVisibility(View.INVISIBLE);
                    viewButton.setText("Grid");
                }
            }
        });
    }

    private Comparator<Student> byName() {
        return new Comparator<Student>() {
            public int compare(Student o1, Student o2) {
                return o1.name.compareToIgnoreCase(o2.name);
            }
        };
    }

    public void sortName() {
        Collections.sort(studentData, byName());
        for (int i = 0; i < studentData.size(); i++) {
            adapter.notifyDataSetChanged();
        }
    }

    private Comparator<Student> byRoll() {
        return new Comparator<Student>() {
            public int compare(Student o1, Student o2) {
                if (o1.getRoll() > o2.getRoll()) {
                    return 1;
                } else {
                    return -1;
                }
            }
        };
    }

    public void sortRoll() {
        Collections.sort(studentData, byRoll());
        for (int i = 0; i < studentData.size(); i++) {
            adapter.notifyDataSetChanged();
        }
    }

    public class DeleteDataTask extends AsyncTask<Void, Void, Void> {

        private final ProgressDialog dialog = new ProgressDialog(MainActivity.this);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.dialog.setMessage("Deleteing data...");
            this.dialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            database.deleteContact(adapter.getItem(menuSelectedItemIndex).getRoll());
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            studentData = database.getUpdateList(studentData);
            adapter.notifyDataSetChanged();
            this.dialog.dismiss();
        }
    }

    public class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action=intent.getExtras().getString("action");
            boolean flag=intent.getExtras().getBoolean("flag");

            if(action.equals("add"))
            {

                    name = intent.getExtras().getString("studentName");
                    imagePath = intent.getExtras().getString("imagePath");
                    rollNo = Integer.parseInt(intent.getExtras().getString("roll"));
                    Student student = new Student(rollNo, name, imagePath);
                    studentData.add(student);
                    Toast.makeText(getApplicationContext(), "Saved Successfully1", Toast.LENGTH_SHORT).show();
                    adapter.notifyDataSetChanged();
            }
            else
            if(action.equals("delete")){
                studentData.remove(menuSelectedItemIndex);
                adapter.notifyDataSetChanged();
            }
            else
            if(action.equals("edit")){
                name = intent.getExtras().getString("studentName");
                imagePath = intent.getExtras().getString("imagePath");
                rollNo = Integer.parseInt(intent.getExtras().getString("roll"));
                adapter.getItem(menuSelectedItemIndex).setName(name);
                adapter.getItem(menuSelectedItemIndex).setImagePath(imagePath);
                adapter.getItem(menuSelectedItemIndex).setRoll(rollNo);
                Toast.makeText(getApplicationContext(), "Updated Successfully", Toast.LENGTH_SHORT).show();
                adapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }
}

